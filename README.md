# Style transformer.

Allows alternate syntax for combining multiple styles without the use of
`compose`.

Usage:

```jsx
className={styles.someclass}
  -> className='someclass'

className={styles.someclass({ otherclass: true })}
  -> className='someclass otherclass'
```

Useful with props:

```jsx
className={styles.someclass({ otherclass: this.props.booleanValue })}
```

Full example:

SASS:

```sass
.header
    font-size: 2em
    font-weight: bold

    &.small
        font-size: 1.5em

.logo
    // Regular styles can be used as usual
```

React Component:

```jsx
// ...
import styles from '.../stylesheet.css'

// ...
  render () {
    return (
      <div className={styles.header({ small: this.props.small })}>
        <div className={styles.logo}></div>
        Example Text
      </div>
    )

  }
// ...
```
