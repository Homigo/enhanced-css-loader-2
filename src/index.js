import cssLoader from 'css-loader'

// Intercept the css-loader output and replace it with the patched styles
const intercept = (content) => {
  // We use a regex to extract the locals from css-loader
  const regex = /exports.locals = ({(.|[\s\S])*});/g
  const extracted = regex.exec(content)

  // Require the input to have css local classes
  if (!extracted || !extracted[1]) {
    throw new Error(
      'Could not extract classNames. Does your CSS file have classes?'
    )
  }

  const transformer = 'require("@homigo/enhanced-css-loader/lib/transform").default'
  const jsPatch = `exports.locals = ${transformer}(${extracted[1]});`

  // Replace the original exports with our modifications
  return content.replace(regex, jsPatch)
}

// Wrap css-loader with `modules: true`
export default function (input) {
  const webpackCallback = this.async()

  const query = {
    // Pass in the rest of the query as `css-loader` options
    ...this.query,
    modules: true,
  }

  this.async = () =>
    (err, content) => {
      if (err) {
        webpackCallback(err)
      } else {
        webpackCallback(null, intercept(content))
      }
    }

  cssLoader.call({ ...this, query }, input)
}
