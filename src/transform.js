// Perform the transformation on the input styles
export default (styles) => {
  const patched = {}

  for (const styleKey of Object.keys(styles)) {
    const patcher = patched[styleKey] = (parameters) => {
      // Set the original style name first
      let combine = styles[styleKey]

      // If we get no parameters return the style anyway
      if (!parameters) {
        return combine
      } else if (typeof parameters !== 'object') {
        throw new Error(`Expected object as argument to ${styleKey}(...)`)
      }

      // For each parameter given, add the css-modules hashed name if the
      // boolean value is true.
      for (const parameterKey of Object.keys(parameters)) {
        if (parameters[parameterKey]) {
          if (!styles[parameterKey]) {
            throw new Error(`Could not find class '${parameterKey}'`)
          }

          combine += ` ${styles[parameterKey]}`
        }
      }

      return combine
    }

    // Set `toString()` to allow usage without manually calling `toString()`
    patcher.toString = () => styles[styleKey]
  }

  return patched
}
